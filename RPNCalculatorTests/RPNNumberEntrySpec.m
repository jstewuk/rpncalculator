//
//  RPNNumberEntrySpec.m
//  RPNCalculator
//
//  Created by James Stewart on 1/7/13.
//  Copyright (c) 2013 StewartStuff. All rights reserved.
//

#import "Kiwi.h"
#import "RPNViewController.h"
#import "RPNCalculator.h"

@interface RPNViewController ()
@property (weak, nonatomic) IBOutlet UILabel *display;
- (IBAction) userDidPressNumber:(UIButton *)sender;
- (IBAction) userDidPressFunction:(UIButton *)sender;
- (void)clearDisplay;
@end

SPEC_BEGIN(RPNNumberEntrySpec)
UIButton * (^buttonWithTitle)(NSString *name) =
    ^UIButton * (NSString* name) {
        UIButton *button = [[UIButton alloc] init];
        [button setTitle:name forState:UIControlStateNormal];
        return button;
    };
__block id calcMoc = nil;
//[[calcMoc should] receive:@selector(enter:)];
RPNViewController * (^calculatorVC)(void) =
    ^RPNViewController * (void) {
        calcMoc = [RPNCalculator nullMockWithName:@"calcMoc"];
        RPNViewController *vc = [[RPNViewController alloc] init];
        vc.calculator = calcMoc;  //setter injection
        UILabel *label = [[UILabel alloc] init];
        [vc.view addSubview:label];
        vc.display = label;
        return vc;
    };
describe(@"Calculator View Controller", ^{
    RPNViewController *calcVC = calculatorVC();
    UIButton *numberThreeButton = buttonWithTitle(@"3");
    UIButton *numberFiveButton = buttonWithTitle(@"5");
    UIButton *enterButton = buttonWithTitle(@"Ent");
    UIButton *plusMinusButton = buttonWithTitle(@"+/-");
    
    beforeEach(^{
        [calcVC clearDisplay];
    });
    
    context(@"when the display is blank", ^{
        it(@"displays 3 after the 3 button is pressed", ^{
            [calcVC userDidPressNumber:numberThreeButton];
            [[calcVC.display shouldNot] beNil];
            [[calcVC.display.text should] equal:@"3"];
        });
        it(@"displays 35 after the 3 button and 5 button are pressed", ^{
            [calcVC userDidPressNumber:numberThreeButton];
            [calcVC userDidPressNumber:numberFiveButton];
            [[calcVC.display.text should] equal:@"35"];
        });
        it(@"returns 5 after the 5 button and the Ent button are pressed", ^ {
            [calcVC userDidPressNumber:numberFiveButton];
            [[calcMoc should] receive:@selector(enter:)];
            [calcVC userDidPressFunction:enterButton];
            [[calcVC.display.text should] equal:@"5"];
        });
        it(@"passes 5 to the calculator after the 5 button and the Ent button are pressed", ^{
            [calcVC userDidPressNumber:numberFiveButton];
            [[calcMoc should] receive:@selector(enter:) withArguments:theValue(5.0f)];
            [calcVC userDidPressFunction:enterButton];
        });
        it(@"passes 35 to the calculator after the 3 button, the 5 button and the Ent button are pressed", ^{
            [calcVC userDidPressNumber:numberThreeButton];
            [calcVC userDidPressNumber:numberFiveButton];
            [[calcMoc should] receive:@selector(enter:) withArguments:theValue(35.0f)];
            [calcVC userDidPressFunction:enterButton];
        });
        it(@"passes 3 to the calculator after the 3 button and the +/- button are pressed", ^{
            [calcVC userDidPressNumber:numberThreeButton];
            [[calcMoc should] receive:@selector(enter:) withArguments:theValue(3.0f)];
            [calcVC userDidPressFunction:plusMinusButton];
        });
        it(@"passes 3 and changeSign to the calculator after the 3 button and the +/- button are pressed", ^{
            [calcVC userDidPressNumber:numberThreeButton];
            [[calcMoc should] receive:@selector(enter:) withArguments:theValue(3.0f)];
            [[calcMoc should] receive:@selector(changeSign)];
            [calcVC userDidPressFunction:plusMinusButton];
        });
        it(@"passes 3 and changeSign to the calculator after the 3 button and the +/- button are pressed and displays -3", ^{
            [calcVC userDidPressNumber:numberThreeButton];
            [[calcMoc should] receive:@selector(enter:) withArguments:theValue(3.0f)];
            [[calcMoc should] receive:@selector(changeSign) andReturn:theValue(-3.0f)];
            [calcVC userDidPressFunction:plusMinusButton];
            [[calcVC.display.text should] equal:@"-3.00"];
        });
        
    });
});
SPEC_END