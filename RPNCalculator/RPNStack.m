//
//  RPNStack.m
//  CalculatorStack
//
//  Created by James Stewart on 1/6/13.
//  Copyright (c) 2013 StewartStuff. All rights reserved.
//

#import "RPNStack.h"

@interface RPNStack ()
@property(nonatomic, retain) NSMutableArray *data;
@end

@implementation RPNStack

- (id)init {
    self = [super init];
    if (self) {
        _data = [[NSMutableArray alloc] initWithCapacity:4];
    }
    return self;
}

- (void)push:(CGFloat)value {
    [self.data addObject:[NSNumber numberWithDouble:value]];
}

- (CGFloat)pop {
    CGFloat popValue = self.top;
    if (! [self.data lastObject]) {
        [NSException raise:@"RPNEmptyStackPopException" format:@"Pop was sent to an empty stack"];
    }
    [self.data removeLastObject];
    return popValue;
}

- (CGFloat)top {
    return [[self.data lastObject] doubleValue];
}

- (void)clear {
    [self.data removeAllObjects];
}

- (NSUInteger)count {
    return [self.data count];
}

@end
