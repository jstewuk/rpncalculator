//
//  RPNAppDelegate.h
//  RPNCalculator
//
//  Created by James Stewart on 1/7/13.
//  Copyright (c) 2013 StewartStuff. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
