//
//  RPNCalculator.m
//  CalculatorStack
//
//  Created by James Stewart on 1/6/13.
//  Copyright (c) 2013 StewartStuff. All rights reserved.
//

#import "RPNCalculator.h"
#import "RPNStack.h"

@interface RPNCalculator ()
@property (nonatomic, strong) RPNStack *stack;
@end

@implementation RPNCalculator

- (id)init {
    self = [super init];
    if (self) {
        _stack = [[RPNStack alloc] init];
    }
    return self;
}

- (void)enter:(CGFloat)value {
    [self.stack push:value];
}

- (void)clear {
    [self.stack clear];
}

- (CGFloat)moveRegistersDown {
    return [self.stack pop];
}

#pragma Unary Operations

- (CGFloat)squareRoot {
    [self.stack push:sqrt([self.stack pop])];
    return [self.stack top];
}

- (CGFloat)square {
    CGFloat val = [self.stack pop];
    [self.stack push:(val * val)];
    return [self.stack top];
}

- (CGFloat)changeSign {
    [self.stack push:(-[self.stack pop])];
    return [self.stack top];
}

- (CGFloat)reciprocal {
    [self.stack push:(1.0/[self.stack pop])];
    return [self.stack top];
}

#pragma Binary Operations

- (CGFloat)add {
    [self.stack push:([self.stack pop] + [self.stack pop])];
    return [self.stack top];
}

- (CGFloat)subtract {
    CGFloat op2 = [self.stack pop];
    [self.stack push:([self.stack pop] - op2)];
    return [self.stack top];
}

- (CGFloat)multiply {
    [self.stack push:([self.stack pop] * [self.stack pop])];
    return [self.stack top];
}

- (CGFloat)divide {
    CGFloat op2 = [self.stack pop];
    [self.stack push:([self.stack pop] / op2)];
    return [self.stack top];
}
@end
