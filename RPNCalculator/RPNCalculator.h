//
//  RPNCalculator.h
//  CalculatorStack
//
//  Created by James Stewart on 1/6/13.
//  Copyright (c) 2013 StewartStuff. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RPNCalculator : NSObject

- (void)enter:(CGFloat)value;
- (void)clear;
- (CGFloat)moveRegistersDown;

- (CGFloat)squareRoot;
- (CGFloat)square;
- (CGFloat)changeSign;
- (CGFloat)reciprocal;


- (CGFloat)add;
- (CGFloat)subtract;
- (CGFloat)multiply;
- (CGFloat)divide;

@end
