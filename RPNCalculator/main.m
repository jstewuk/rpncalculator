//
//  main.m
//  RPNCalculator
//
//  Created by James Stewart on 1/7/13.
//  Copyright (c) 2013 StewartStuff. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RPNAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RPNAppDelegate class]));
    }
}
