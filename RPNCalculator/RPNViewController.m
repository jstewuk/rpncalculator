//
//  RPNViewController.m
//  RPNCalculator
//
//  Created by James Stewart on 1/7/13.
//  Copyright (c) 2013 StewartStuff. All rights reserved.
//

#import "RPNViewController.h"
#import "RPNCalculator.h"

@interface RPNViewController ()
@property (weak, nonatomic) IBOutlet UILabel *display;

- (IBAction)userDidPressFunction:(id)sender;
- (void)clearDisplay;
@end

@implementation RPNViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.display.text = @"";
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) userDidPressNumber:(UIButton*)sender {
    self.display.text = [self.display.text stringByAppendingString:[sender titleForState:UIControlStateNormal]];
}

- (IBAction) userDidPressFunction:(UIButton *)sender {
    double number = [self.display.text doubleValue];
    [self.calculator enter:number];
    if (![sender.titleLabel.text isEqualToString:@"Ent"]) {
        self.display.text = [NSString stringWithFormat:@"%.2f",[self.calculator changeSign]];
    }
}

- (void)clearDisplay {
    self.display.text = @"";
}
@end
