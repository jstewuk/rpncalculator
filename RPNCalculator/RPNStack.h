//
//  RPNStack.h
//  CalculatorStack
//
//  Created by James Stewart on 1/6/13.
//  Copyright (c) 2013 StewartStuff. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RPNStack : NSObject

- (void)push:(CGFloat)value;
- (CGFloat)pop;
- (CGFloat)top;
- (void)clear;

@end
