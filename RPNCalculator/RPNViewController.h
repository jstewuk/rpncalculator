//
//  RPNViewController.h
//  RPNCalculator
//
//  Created by James Stewart on 1/7/13.
//  Copyright (c) 2013 StewartStuff. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPNCalculator;

@interface RPNViewController : UIViewController
@property (strong, nonatomic) IBOutlet RPNCalculator *calculator;

@end
